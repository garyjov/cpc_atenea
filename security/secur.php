<?php
/*---------------------------------------------------------------------------------------------*/
#Titulo:Sguridad de campos
/*---------------------------------------------------------------------------------------------*/
#►Descripcion:Clase para limpair campos y evitar inyecciones sql
#►Clase: Seguridad
class Seguridad{	

	#►Metodo: cleanData()->Limpia datos ingresados en el form de empresas---------------------
	public function cleanData($dato){
		//
		$dato = trim($dato);
		//
		$dato = stripcslashes($dato);
		//
		$dato = htmlspecialchars($dato);
		//Retorno del dato
		return $dato;
	}
}
/*Fin------------------------------------------------------------------------------------------*/