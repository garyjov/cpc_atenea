<?php 
#¶►(Nota): Archivo Index para el llamado de las Vistas◄#
/*---------------------------------------------------------------------*/
#►Llamado de Clases
/*---------------------------------------------------------------------*/
#¶►(Controladores): Controlador del Template y Links de Navegacion◄#
require_once "controllers/controller.php";
#¶►(Modelos): Modelo del Template y Links de Navegacion◄#
require_once "models/model.php";

#¶►(Comment): INSTACIA DE CLASE CONTROLADOR DEL TEMPLATE--------------◄#
// Isntacia
$objTemplate = new TemplateController();
// Llamada del intro
$objTemplate->GetTemplate();