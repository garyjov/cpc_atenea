/*---------------------------------------------------------------------------------------------*/
//►Titulo:LOGICA DEL MAPA
/*---------------------------------------------------------------------------------------------*/
/*►Descripcion:
	Script para el control de la lógica del mapa de regiones
*/
function CambioMapa(img){
	//Identificacion del contenedor del mapa
	$("#mp-regiones").css("background-image", "url(views/css/Mapa/"+ img + ".png)");
}
function RestMapa(){
	$("#mp-regiones").css("background-image", "url(views/css/Mapa/fondo.png)");
}
/*Fin------------------------------------------------------------------------------------------*/