 //
 $(document ).ready(function() {
    $("#login").submit(function(e){
    e.preventDefault();
    log();
  });
    $("#restore").submit(function(e){
    e.preventDefault();
    restore();
  });
});

function log(){

	$("#response").fadeOut()
	//Crear permiso para envio de datps
	permiso = 0;
	//Referencia la variable de las respuetas
	respuestas = document.getElementById('login');
	//expresion regular email
  emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	//Referenciar los inputs
	inpLog = document.querySelectorAll(".form-control");
	//Recorrer el array de inputs
	for(i = 0; i < inpLog.length; i++){
		
		//Verificar si algun input esta vacio
		if(inpLog[i].value == ""){
			//Identificar el campo vacio y enviar mensaje placehoder
			inpLog[i].placeholder = "Completa este campo....";
			inpLog[i].style.borderColor="red"
			//Focus del ELemento
			inpLog[i].focus();
			// Retornar falso para eviar enviar el formulario
			return false;
		
		}else{

			//Aumentar el permiso para el envio de datos
			permiso ++;
		
		}

	}

	if(!emailRegex.test(document.getElementById('user').value))
	{
	   document.getElementById('user').placeholder = "Correo inválido";
	   document.getElementById('user').style.borderColor="red"
	   permiso -= 1;
	}
	//Validar el permiso para enviar los datos
	if(permiso == 2){
		$.ajax({
//HEAD
	    type:"post",
	    url:"views/filters/filterLogin.php",
	    data:$("#login").serialize(),
	    success:function(e){
				
				if(String(e).indexOf("denegado") != -1)
				{						
					$("#response").html("Datos de acceso incorrectos")
					$("#response").fadeIn()
					$("#response").css("background-color","#F48A73")
				}
				else if(String(e).indexOf("usuario bloqueado") != -1)
				{
					$("#response").html("El usuario fue bloqueado, contacta al administrador al correo operador.idt@cpcoriente.org")
					$("#response").fadeIn()
					$("#response").css("background-color","#F48A73")
				}
				else
				{
					var respuesta=JSON.parse(e)
					if(String(respuesta[0]).indexOf("exitoso") != -1){
					$("#response").html("Acceso exitoso")
					$("#response").fadeIn()
					$("#response").css("background-color","#C3E98D")
					window.location.href="backend/index.php?ruta="
				}


				}
	  	}
		});

			   
// 0c2285e7da1f43e3a6ec9c87c649cbc0bf30abe6


	}else{
		//alert("Tranquilo Pillo... i can see you");
		return false;
	}

}
function restore(){

	$("#response").fadeOut()
	//Crear permiso para envio de datps
	permiso = 0;
	//Referencia la variable de las respuetas
	respuestas = document.getElementById('restore');
	//expresion regular email
    emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
	//Referenciar los inputs
	inpLog = document.querySelectorAll(".form-control");
	//Recorrer el array de inputs
	for(i = 0; i < inpLog.length; i++){
		
		//Verificar si algun input esta vacio
		if(inpLog[i].value == ""){
			//Identificar el campo vacio y enviar mensaje placehoder
			inpLog[i].placeholder = "Completa este campo....";
			inpLog[i].style.borderColor="red"
			//Focus del ELemento
			inpLog[i].focus();
			// Retornar falso para eviar enviar el formulario
			return false;
		}
       
		else{

			//Aumentar el permiso para el envio de datos
			permiso ++;
		
		}

	}

	if(!emailRegex.test(document.getElementById('email').value))
        		{
                 document.getElementById('email').placeholder = "Correo inválido";
                 document.getElementById('email').style.borderColor="red"
                 permiso-=1
        		}
	//Validar el permiso para enviar los datos
	if(permiso == 1){
		$.ajax({
			    type:"post",
                url:"views/filters/filterLogin.php",
                data:$("#restore").serialize(),
                success:function(e)
                	{
					
					if(String(e).indexOf("enviada")==-1)
					{
						$("#response").css("background-color","#F48A73")
					}	
					else
					{
					   	$("#response").css("background-color","#C3E98D") 
					}
								
					$("#response").html(e)
					$("#response").fadeIn()
					
						
						

                	}

				})


	}else{
		alert("Tranquilo Pillo... i can see you");
		return false;
	}

}

function EstadoNormal(id){
	var texto=""
if(id=="user")texto="Usuario"
	else texto="Contraseña"
	$("#"+id).attr("placeholder",texto)
	$("#"+id).css("border-color", "#0099A5")

}