//https://gijgo.com/ - Libreria MIT
//FUNCION PARA EL DATE PIKCER
//--------------------------------------------------------------------------
$('#datepicker').datepicker({
	uiLibrary: 'bootstrap4',
	inconsLibrary: 'fontawesome',
	modal: true,
	header: true,
	footer: true
});
//--------------------------------------------------------------------------

//FUNCION PARA EL DATE TIMEPICKER
//--------------------------------------------------------------------------
$('#timepicker').timepicker({
	uiLibrary: 'bootstrap4',
	iconsLibrary: 'fontawesome',
	modal: true,

})
//--------------------------------------------------------------------------