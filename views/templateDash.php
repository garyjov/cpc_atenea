<!DOCTYPE html>
<html lang="es">
<head>
	<!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Cpc-Data</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
  <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="css/style-casc.css">
</head>
<body>
	<!-- Barra Navegacion Lateral----------------------------------------------------------------------------------- -->
	<div class="sidenav bg-light shadow-sm">
		<!-- Titulo Barra -->
		<div class="hd-nav bg-dark text-center clearfix">
			<a href="index.html">
				<img src="assets/logo-at-1.png" class="float-left rounded-circle img-tit" width="50px" height="50px">
			</a>
			<h4 class="float-right tit">Atenea</h4>
		</div>
		<!-- Brupo de Botones -->
		<div class="btn-group-vertical mt-3 bt-grp 	btn-block" style="padding: 0 10px	">
		  <button type="button" class="btn btn-outline-dark" data-toggle="collapse" data-target="#dibug">Divulgación</button>
		  <div class="collapse" id="dibug">
		  	<div class="btn-group-vertical mt-2 mb-2 btn-block">
		  		<a href="eventos.html" class="btn btn-outline-secondary">Eventos</a>
		  		<a href="calendario.html" class="btn btn-outline-secondary">Calendario</a>
		  		<a href="prereg.html" class="btn btn-outline-secondary">Registros</a>
		  		<a href="asistencia.html" class="btn btn-outline-secondary">Asistencias</a>
		  		<a href="##" class="btn btn-outline-secondary">Reportes</a>
		  	</div>
		  </div>
		  <button type="button" class="btn btn-outline-dark" data-toggle="collapse" data-target="#caracte">Caracterización</button>
		  	<div class="collapse" id="caracte">
			  	<div class="btn-group-vertical mt-2 mb-2 btn-block">
			  		<a href="eventos.html" class="btn btn-outline-secondary">Empresas</a>
			  		<a href="##" class="btn btn-outline-secondary">Grupos</a>
			  		<a href="##" class="btn btn-outline-secondary">Calendario</a>
			  		<a href="##" class="btn btn-outline-secondary">Reportes</a>
			  	</div>
		  	</div>
		  <button type="button" class="btn btn-outline-dark" data-toggle="collapse" data-target="#formac">Formación</button>
		  	<div class="collapse" id="formac">
		  		<div class="btn-group-vertical mt-2 mb-2 btn-block">
		  			<a href="##" class="btn btn-outline-secondary">Talleres</a>
		  			<a href="##" class="btn btn-outline-secondary">Acompañamiento</a>
		  			<a href="##" class="btn btn-outline-secondary">Evidencias</a>
		  			<a href="##" class="btn btn-outline-secondary">Asistencias</a>
		  			<a href="##" class="btn btn-outline-secondary">Resultados</a>
		  		</div>
		  	</div>
		  <button type="button" class="btn btn-outline-dark" data-toggle="collapse" data-target="#resul">Resultados</button>
		  <div class="collapse" id="resul">
		  		<div class="btn-group-vertical mt-2 mb-2 btn-block">
		  			<a href="##" class="btn btn-outline-secondary">Evidencias</a>
		  			<a href="##" class="btn btn-outline-secondary">Listas</a>
		  		</div>
		  	</div>
		</div>
	</div>
	<!-- ----------------------------------------------------------------------------------------------------------- -->

		<!-- Contenedor Barra navegacion Superior--------------------------------------------------------------------->
	<div class="cont-casc" style="">
		<div class="bg-light">
			<!-- Barra de Navegación-------------------------------- -->
			<nav class="text-light navbar navbar-expand-sm bg-secondary justify-content-end" style="height: 100%;padding: 18px 10px;font-size: 22px">
			 <ul class="nav">
			 	<li class="nav-item mr-4">
			 		Andres Garcia
			 	</li>
			 	<li class="nav-item text-primary">
			 		<a href="##" class="text-light">
			 			<i class="fas fa-power-off"></i>
			 		</a>
			 	</li>
			 </ul>
			</nav>
		</div>
		<!-- ------------------------------------------------------------------------------------------------------ -->

		<!-- Contenedor para el cuerpo----------------------------------------------------------------------------  -->
		<div class="cont-bdy">
			<!-- Contenido del Index -->
			<div class="container">
				<div class="cont-tit-ind text-center bg-light pt-3 pb-3">
					<h4>Atenea</h4>
					<p class="small">Selecciona una herramienta</p>
				</div>
				<div class="card-deck">
					<div class="card bg-light">
						<div class="card-body text-center">
							<h5>Divulgación</h5>
							<div class="btn-group-vertical mt-2 btn-block">
								<a class="btn btn-outline-secondary">Eventos</a>
								<a class="btn btn-outline-secondary">Calendario</a>
								<a class="btn btn-outline-secondary">Registros</a>
								<a class="btn btn-outline-secondary">Asistencias</a>
								<a class="btn btn-outline-secondary">Reportes</a>
							</div>
						</div>
					</div>
					<div class="card bg-light">
						<div class="card-body text-center">
							<h5>Caracterización</h5>
							<div class="btn-group-vertical mt-2 btn-block">
								<a class="btn btn-outline-secondary">Empresas</a>
								<a class="btn btn-outline-secondary">Grupos</a>
								<a class="btn btn-outline-secondary">Listas</a>
								<a class="btn btn-outline-secondary">Asistecias</a>
							</div>
						</div>
					</div>
					<div class="card bg-light">
						<div class="card-body text-center">
							<h5>Formación</h5>
							<div class="btn-group-vertical btn-block">
								<a class="btn btn-outline-secondary">Talleres</a>
								<a class="btn btn-outline-secondary">Acompañamiento</a>
								<a class="btn btn-outline-secondary">Evidencias</a>
								<a class="btn btn-outline-secondary">Asistencias</a>
								<a class="btn btn-outline-secondary">Resultados</a>
							</div>
						</div>
					</div>
					<div class="card bg-light">
						<div class="card-body text-center">
							<h5>Resultados</h5>
							<div class="btn-group-vertical btn-block">
								<a class="btn btn-outline-secondary">Evidencias</a>
								<a class="btn btn-outline-secondary">Listas</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ------------------------------------------------------------------------------------------------------ -->

	</div>
	<!-- -------------------------------------------------------------------------------------------------------- -->


	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="js/dates.js"></script>
</body>
</html>