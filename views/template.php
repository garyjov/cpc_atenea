<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="icon" href="views/assets/icon-tit.png">
	<title>CPC Desarrollo</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- BOOTSTRAP 4---------------------------------------------------------------------- -->
	<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

	<!-- FONT AWESOME 5--------------------------------------------------------- -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<!-- JQUERY--------------------------------------------------------------------------- -->
	<script src="views/js/jquery.js"></script>
	<!-- STYLE CSS------------------------------------------------------------------------ -->
	<link rel="stylesheet" href="views/css/style.css">
	<link rel="stylesheet" href="views/css/querys.css">
</head>
<body class="fade-in">
	<?php 
		if(isset($_GET['ruta'])){
			
			if($_GET['ruta'] == "que" || $_GET['ruta'] == "como" || $_GET['ruta'] == "cpc" || $_GET['ruta'] == "cuando" || $_GET['ruta'] == "contacto"){
				#¶►(Comment): INSTACIA DEL CONTROLADOR DE MODULOS DE VISTA◄#
				include('views/modules/cabezote.php');
				//Instacia
				$objNavigator = new TemplateController();
				//Llamado del metodo de navegacion
				$objNavigator->urlController();
				// Incluir Modulo Footer 
				include('views/modules/footer.php');

			}else if($_GET['ruta'] == "backlog"  || $_GET['ruta'] == "restaurar_pass"  ){
				//Instancia
				$objNavigator = new TemplateController();
				//Llamado del metodo de navegacion
				$objNavigator->urlController();
				
			}
		}else{

			include('views/modules/intro.php');
		}
	?>
	<!-- BOOTSTRAP 4 BUNDLE Y  JS--------------------------------------------------------- -->
	<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- SCRIPTS JS----------------------------------------------------------------------- -->
	<!-- Logica del Mapa -->
	<script src="views/js/logMapa.js"></script>
</body>
</html>