<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## BARRA DE NAVEGACIÓN -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar navbar-expand-md justify-content-center bck-navbar sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon bt-nav-res" style="color:#fff"><strong>O</strong></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav text-center bar-list">
			<li class="nav-item"><a href="index.php?ruta=que" class="nav-link linkstext-center">¿Qué?</a></li>
			<li class="nav-item"><a href="index.php?ruta=como" class="nav-link links text-center">¿Cómo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cpc" class="nav-link links text-center">¿CPC Oriente?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cuando" class="nav-link links text-center link-active">¿Cuándo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=contacto" class="nav-link links text-center">¿Contacto?</a></li>
		</ul>
  </div>
</nav>
<br>
<br>
<!-- -------------------------------------------------------------------------------------------------------- -->

<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## CONTENIDO DIVULCACIÓN  -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<div class="container">
	<!-- TITULO-------------------------------------- -->
	<div class="row">
		<div class="col-12">
			<h4 class="txt-cpc-1">Divulgación</h4>
			<hr>
			<p class="text-justify txt-cpc-2">El Proceso de <strong>DIVULGACIÓN</strong> sera dividido por las <strong>6 regiones</strong> componentes del país y a su vez sera realizado en los departamentos clave correspondientes a cada region.</p>
			<p class="txt-cpc-2">La Inscripción a su proceso la podra realizar en este portal siguiendo los 4 pasos descritos a continuación.</p>
			<hr>
		</div>
	</div>
	<!-- INSTRUCCIONES-------------------------------------- -->
	<div class="row txt-cpc-2">
		<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
			<div class="cont-instruc-p1">
				<h1 class="txt-cpc-1">1<span class="small">er</span>.Paso</h1>
				<p><strong>Seleccione</strong> una <strong>Región</strong> del mapa dando click.</p>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
			<div class="cont-instruc-p2">
				<h1 class="txt-cpc-1">2<span class="small">do</span>.Paso</h1>
				<p><strong>Seleccione</strong> una <strong>Departamento</strong> de la lista.</p>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
			<div class="cont-instruc-p3">
				<h1 class="txt-cpc-1">3<span class="small">er</span>.Paso</h1>
				<p><strong>Seleccione</strong> el <strong>Lugar</strong> mas cercano a su localidad en la lista.</p>
			</div>
		</div>
		<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3">
			<div class="cont-instruc-p4">
				<h1 class="txt-cpc-1">4<span class="small">to</span>.Paso</h1>
				<p><strong>Inscríbase</strong> y/o <strong>Présentese</strong> 15 minutos antes del proceso.</p>
			</div>
		</div>
	</div>
	<hr>	
	<div class="cont-mapa-regiones">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
				<div class="cont-tit-reg">
					<h4 class="txt-cpc-2"><strong>Región:</strong> <span class="txt-cpc-1" id="txt-region">Selecciona una Región</span></h4>
				</div>
				<br>
				<div class="cont-map">
					<!-- Imagen Mapa de REgiones -->
					<div class="map-regiones" id="mp-regiones"> 	
						<i class="fas fa-map-marker-alt p-map-amazo" onmouseover="CambioMapa('regAmazonia')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Amazonia</span></i>
						<i class="fas fa-map-marker-alt p-map-cent" onmouseover="CambioMapa('regCentral')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Central</span></i>
						<i class="fas fa-map-marker-alt p-map-orin" onmouseover="CambioMapa('regOrinoquia')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Orinoquia</span></i>
						<i class="fas fa-map-marker-alt p-map-pacifi" onmouseover="CambioMapa('regPacifico')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Pacifico</span></i>
						<i class="fas fa-map-marker-alt p-map-antieje" onmouseover="CambioMapa('regAntiEje')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Antioquia-Eje Caf.</span></i>
						<i class="fas fa-map-marker-alt p-map-santan" onmouseover="CambioMapa('regSantanderes')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Santanderes</span></i>
						<i class="fas fa-map-marker-alt p-map-cari" onmouseover="CambioMapa('regCaribe')" onmouseout="RestMapa()"><br><span style="font-size: 12px">Caribe</span></i>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<!-- --------------------------------------------------------------------------------------------------------