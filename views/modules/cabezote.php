<!-- ------------------------------------------------------------------------------------------------------- -->
<!-- ## HEADER PORTADA -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<section class="hd-inicio">
	<div class="hd-logo text-center">
		<img class="img-anim q-img-h1" src="views/assets/sena.png">
		<img class="img-anim q-img-h2" src="views/assets/colciencias.png" >
		<img class="img-anim q-img-h3" src="views/assets/logo_CPC_Ppal.png">
	</div>
</section>