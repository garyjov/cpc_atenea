<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## BARRA DE NAVEGACIÓN -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar navbar-expand-md justify-content-center bck-navbar sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon bt-nav-res" style="color:#fff"><strong>O</strong></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav text-center bar-list">
			<li class="nav-item"><a href="index.php?ruta=que" class="nav-link linkstext-center">¿Qué?</a></li>
			<li class="nav-item"><a href="index.php?ruta=como" class="nav-link links text-center link-active">¿Cómo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cpc" class="nav-link links text-center">¿CPC Oriente?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cuando" class="nav-link links text-center">¿Cuándo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=contacto" class="nav-link links text-center">¿Contacto?</a></li>
		</ul>
  </div>
</nav>
<br>
<!-- ----------------------------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION-1  : PASOS PARA CONVOCATORIA-------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<div class="container">
	<!-- Paso 1 -->
	<div class="row">
		<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<h1 class="display-1 text-right" style="color:#666"><strong>1.</strong></h1>
		</div>
		<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
			<h4><strong class="txt-cpc-1">LEA</strong> la informacion descrita en la sección <strong class="txt-cpc-1">¿Qué?</strong>.</h4>
			<a href="index.php?ruta=que" target="_blank">
				<h2><span class="badge badge-info">Ver Sección</span></h2>
			</a>
			<p>Sobre el OBJETIVO y a QUIÉN va dirigida esta convocatoria.</p>
		</div>
	</div>
	<hr>
	<!-- Paso 2 -->
	<div class="row">
		<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8 text-right">
			<h4><strong class="txt-cpc-1">LEA</strong> y <strong class="txt-cpc-1">DESCARGUE</strong> los Términos de referencia</h4>
			<a href="views/pbl_download/terminos.pdf" target="_blank">
				<h2><span class="badge badge-info">Descargar</span></h2>
			</a>
			<p>En ellos encotrara información mas detallada.</p>
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<h1 class="display-1 text-left"><strong>2.</strong></h1>
		</div>
	</div>
	<hr>
	<hr>
	<!-- Paso 3 -->
	<div class="row">
		<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<h1 class="display-1 text-right" style="color:#666"><strong>3.</strong></h1>
		</div>
		<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
			<h4><strong class="txt-cpc-1">CONOZCA</strong> el <strong class="txt-cpc-1">ARTICULADOR</strong> que lo guiara en el proceso</h4>
			<a href="index.php?ruta=cpc" target="_blank">
				<h2><span class="badge badge-info">Conocer</span></h2>
			</a>
			<p>CPC Oriente</p>
		</div>
	</div>
	<hr>
	<!-- Paso 4 -->
	<div class="row">
		<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8 text-right">
			<h4>Este pendiente a las <strong class="txt-cpc-1">FECHAS</strong> de <strong class="txt-cpc-1">DIVULGACIÓN</strong>.</h4>
			<a href="index.php?ruta=cuando" target="_blank">
				<h2><span class="badge badge-info">Ver Divulgación</span></h2>
			</a>
			<p>Se realizaran en las 6 regiones del país.</p>
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<h1 class="display-1 text-left"><strong>4.</strong></h1>
		</div>
	</div>
	<hr>
	<!-- Paso 5 -->
	<div class="row">
		<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<h1 class="display-1 text-right" style="color:#666"><strong>5.</strong></h1>
		</div>
		<div class="col-sm-8 col-md-8 col-lg-8 col-xl-8">
			<h4><strong class="txt-cpc-1">ÍNSCRIBASE</strong> al evento de divulgación de su <strong class="txt-cpc-1">REGIÓN</strong>.</h4>
			<a href="index.php?ruta=cuando" target="_blank">
				<h2><span class="badge badge-info">Inscribirme</span></h2>
			</a>
			<p>Las Inscripciones se podran realizar por medio de este portal.</p>
		</div>
	</div>
	<br>
</div>
<!-- -------------------------------------------------------------------------------------------------------- -->