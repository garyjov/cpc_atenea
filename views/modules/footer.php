<!-- ----------------------------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION : FOOTER -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<footer class="foot">
	<!-- Derechos de Autor -->
	<div class="container text-center">
		<p class="txt-copy">COPYRIGHT&#169; 2019.DERECHOS RESERVADOS <a href="http://www.cpcoriente.org" class="copy-link"><strong>CPCORIENTE</strong></a></p>
	</div>
	<div class="container">
		<div class="row">
			<!-- Contacos -->
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
				<p class="q-tfo-1">
					<span style="font-size: 15px">Contáctenos</span>
					<br>
					<span class="small"><strong>Email: </strong>operador.idt@cpcoriente.org</span>
				</p>
			</div>
			<!-- Mapa del Sitio -->
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
				<p class="q-tfo-2">
					<span style="font-size: 15px">Gestores</span>
					<br>
					<a href="index.php?ruta=backlog" class="text-decoration-none small" style="color:#fff">Atenea Log</a>
				</p>
			</div>
			<!-- Redes Sociales -->
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
				<p class="q-tfo-3">
					<span style="font-size: 15px">Siguenos en</span>
					<br>
					<a href="" class="text-decoration-none">
						<i class='fab fa-facebook' style='font-size:25px;color:#fff;margin-right: 10px'></i>
					</a>
					<a href="" class="text-decoration-none">
						<i class='fab fa-twitter-square' style='font-size:25px;color:#fff;margin-right: 10px'></i>
					</a>
					<a href="" class="text-decoration-none">
						<i class='fab fa-youtube' style='font-size:25px;color:#fff;margin-right: 10px'></i>
					</a>
				</p>
			</div>
		</div>
	</div>
</footer>
<!-- -------------------------------------------------------------------------------------------------------- -->