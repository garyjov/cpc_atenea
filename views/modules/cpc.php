<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## BARRA DE NAVEGACIÓN -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar navbar-expand-md justify-content-center bck-navbar sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon bt-nav-res" style="color:#fff"><strong>O</strong></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav text-center bar-list">
			<li class="nav-item"><a href="index.php?ruta=que" class="nav-link linkstext-center">¿Qué?</a></li>
			<li class="nav-item"><a href="index.php?ruta=como" class="nav-link links text-center">¿Cómo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cpc" class="nav-link links text-center link-active">¿CPC Oriente?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cuando" class="nav-link links text-center">¿Cuándo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=contacto" class="nav-link links text-center">¿Contacto?</a></li>
		</ul>
  </div>
</nav>
<br>
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION : QUE ES EL CPC ORIENTE -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
	<section class="container">
		<!-- Titulo de Seccion -->
		<div style="border-bottom: 1px solid #D5D5D5">
			<h3 class="txt-tit-cpc">¿QUÉ ES EL CPC ORIENTE?</h3>
			<p>Centro de Productividad y Competitividad del Oriente</p>
		</div>
		<br>
		<!-- Contenido CPC 1 -->
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" style="border:1px solid #D5D5D5">
				<img src="views/assets/cpc_Portada.jpg" width="100%" class="img-cent-inno">	
			</div>
			<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 txt-cpc-2 text-justify">
				<p>
					<strong>CPC Oriente</strong> - Es el primer Centro de Innovación y Productividad reconocido por
					Colciencias, con un grupo de investigación categoría B, denominado Gestión
					Tecnológica, Innovación y Ciencia – GTIC.
				</p>
				<p>
					Es un articulador de la dinámica empresarial, para contribuir con el
					mejoramiento productivo y competitivo de los sectores tradicionales y/o promisorios del
					Oriente Colombiano, a través de la generación e implementación de soluciones
					integrales en asistencia técnica, consultoría especializada, proyectos de Ciencia,
					Tecnología e Innovación - CTeI;
				</p>
			</div>
		</div>
		<hr>
		<!-- Contenido CPC 2 -->
		<div class="row">
			<div class="col-12">
				<h3 class="text-justify txt-cpc-2 q-txt-fin">
					“<strong>Contribuimos</strong> al <strong>crecimiento</strong> de las <strong>empresas colombianas</strong> mediante la gestión
					de <strong>proyectos</strong>, gestión de la <strong>innovación</strong> y la gestión de la <strong>productividad</strong>,
					generando empresas más <strong>competitivas</strong>”.
				</h3>
			</div>
		</div>
		<hr>
		<!-- Contenido CPC 3 -->
		<div class="row txt-cpc-2">
			<div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center" style="border-right:1px solid #D5D5D5">
				<img src="views/assets/gestion_B.png" width="100%" style="margin-top: 15px" class="img-equip">	

			</div>
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 text-justify">
				<p>
					<strong><span class="txt-cpc-1">EQUIPO</span></strong> <br>	
					Soportado en un equipo multidisciplinario, competente
					y comprometido; infraestructura física; recursos tecnológicos; el mejoramiento continuo
					de los procesos; la calidad de los servicios y un grupo de investigación reconocido por
					Colciencias.
				</p>
			</div>
			<div class="col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center" style="border-right:1px solid #D5D5D5">
				<img src="views/assets/proceso_B.png" width="110%" style="margin-top: 10px;" class="img-aliado">	
			</div>
			<div class="col-sm-4 col-md-4 col-lg-4 col-xl-4 text-justify" style="">
				<p>
					<strong><span class="txt-cpc-1">ALIADO</span></strong><br>
					Como aliado estratégico, propicia la
					identificación de oportunidades, la formulación y ejecución de proyectos de
					Investigación Aplicada, Desarrollo y Transferencia Tecnológica, Formación
					Especializada a la Medida, Consultoría y Asesoría Especializada.
				</p>
			</div>
		</div>
		<hr>
		<!-- Contenido CPC 5 -->
		<div class="row">
			<div class="col-12">
				<h4 class="txt-cpc-1 text-justify">Nuestro Papel</h4>
				<p class="txt-cpc-2">El <strong>CPC Oriente</strong> tendra el papel de sensibilizar y divulgar a las empresas colombianas la convocatoria de apoyo a proyectos del sector productivo que contribuyan al fortalecimiento de capacidades en I + D + i y presentar a los actores del SNCTI existentes para la conformación de alianzas que impulsen el desarrollo de proyectos que incidan sobre la productividad de las empresas, incluidas las instancias del Sena.</p>
				<hr>
			</div>
		</div>
		<!-- Contenido CPC 4 -->
		<div class="row">
			<div class="col-12">
				<h3 class="text-justify txt-cpc-2 q-txt-desa">
					<strong>Desarrollo Productivo e Innovación</strong>, que atiendan y den solución a las necesidades de aliados y/o clientes (entidades públicas y/o privadas) a nivel regional y nacional.
			</div>
		</div>
		
		<br>
	</section>
<!-- --------------------------------------------------------------------------------------------------------