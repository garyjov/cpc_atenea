<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## BARRA DE NAVEGACIÓN -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar navbar-expand-md justify-content-center bck-navbar sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon bt-nav-res" style="color:#fff"><strong>O</strong></span>
  </button>
  <div class="collapse navbar-collapse text-center" id="collapsibleNavbar">
		<ul class="navbar-nav text-center bar-list">
			<li class="nav-item"><a href="index.php?ruta=que" class="nav-link links link-active text-center">¿Qué?</a></li>
			<li class="nav-item"><a href="index.php?ruta=como" class="nav-link links text-center">¿Cómo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cpc" class="nav-link links text-center">¿CPC Oriente?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cuando" class="nav-link links text-center">¿Cuándo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=contacto" class="nav-link links text-center">¿Contacto?</a></li>
		</ul>
  </div>
</nav>
<br>
<!-- ----------------------------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION-1  : TITULO INFORMACION GENERAL -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<section>
	<div class=" text-center">
		<h3 class="txt-s1-tit">CONVOCATORIA LÍNEA DE FOMENTO A LA INNOVACIÓN Y DESARROLLO <br>
				TECNOLÓGICO EN LAS EMPRESAS <br>
				COLOMBIA 2019
		</h3>
	</div>
</section>
<!-- -------------------------------------------------------------------------------------------------------- -->

<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION-2  : CONTENIDO CONVOCATORIA -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<section class="container cont-sec-2" style="width: 75%">
	<hr>
	<!-- Columnas Contenido Convocatoria-->
	<div class="row">
		<!-- Columna 1 OBJETIVOS-->
		<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="container cl-bor">
				<!-- Titulo -->
				<div class="cl-tit">
					<img src="views/assets/iconos-03.png" class="q-img-obj">
					<h1 class="float-left q-tit-obj" style="margin-left: 10px;color: #004889">OBJETIVOS:</h1> 
				</div>
				<!-- Contenido -->
				<div style="clear:both">
					<p class="cont-obje">
						Apoyar proyectos de Desarrollo Tecnológico e Innovación para ser ejecutados por empresas legalmente constituidas en Colombia, que den como resultado un prototipo funcional con validación pre-comercial, contribuyendo a mejorar su productividad, a sofisticar su oferta productiva, y a fortalecer las alianzas y/o vínculos con los diferentes actores del SNCTeI a nivel nacional.
					</p>
				</div>
			</div>
		</div>
		<!-- Columna 2 DIRIGIDO A-->
		<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6">
			<div class="container cl-bor">
				<!-- Titulo -->
				<div class="cl-tit">
					<img src="views/assets/iconos-07.png" class="q-img-diri">
					<h1 class="float-left q-tit-diri" style="margin-left: 10px;color: #004889">DIRIGIDO A:</h1> 
				</div>
				<!-- Contenido -->
				<div style="clear:both">
					<p class="cont-dirgi">
						Empresas micro, pequeñas, medianas o grandes que tengan identificado un proyecto de Desarrollo Tecnológico e Innovación (a partir del TRL 4), orientado a diversificar, mejorar y sofisticar su oferta productiva.
					</p>
				</div>
			</div>
			<div>
				<abbr class="small">Visite la Página de Colciencias para descargar la información sobre esta convocatoria.</abbr>
				<a href="https://colciencias.gov.co/convocatorias/innovacion/convocatoria-linea-fomento-la-innovacion-y-desarrollo-tecnologico-en-las" target="_blank" style="text-decoration: none!important;">
					<div class="bt-term text-center">
						<h4>Visitar</h4>
					</div>
				</a>
			</div>
		</div>
	</div>
	<br>
	<!-- Oferta Económica Cofinanciación-->
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
			<hr>
			<h2 class="txt-cofina">
				COFINANCIACIÓN HASTA
			</h2>
			<h1 class="text-center txt-cof-pla">&#36;200'000.000</h1>
			<hr>
		</div>
	</div>
	<!--  -->
	<div class="row">
		
	</div>
</section>
<br>
<!-- -------------------------------------------------------------------------------------------------------- -->