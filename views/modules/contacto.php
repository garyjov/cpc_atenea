<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## BARRA DE NAVEGACIÓN -------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<nav class="navbar navbar-expand-md justify-content-center bck-navbar sticky-top">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon bt-nav-res" style="color:#fff"><strong>O</strong></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		<ul class="navbar-nav text-center bar-list">
			<li class="nav-item"><a href="index.php?ruta=que" class="nav-link linkstext-center">¿Qué?</a></li>
			<li class="nav-item"><a href="index.php?ruta=como" class="nav-link links text-center">¿Cómo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cpc" class="nav-link links text-center">¿CPC Oriente?</a></li>
			<li class="nav-item"><a href="index.php?ruta=cuando" class="nav-link links text-center">¿Cuándo?</a></li>
			<li class="nav-item"><a href="index.php?ruta=contacto" class="nav-link links text-center link-active">¿Contacto?</a></li>
		</ul>
  </div>
</nav>
<br>
<br>
<!-- -------------------------------------------------------------------------------------------------------- -->
<!-- ## SECCION  : CONTACTOS CPC-------------------- ## -->
<!-- -------------------------------------------------------------------------------------------------------- -->
<div class="container">
	<div class="row">
		<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6" style="padding: 10px;border-right: 1px solid #D5D5D5">
			<div class="container txt-cpc-2" style="padding: 10px;border:1px solid #D5D5D5">
				<p>
					Si tiene alguna inquietud, solicitud o desea más información, por favor comuníquese con nosotros y le daremos pronta respuesta.
				</p>
			</div>
			<!-- Datos de Contacto -->
			<div class="container" style="padding: 15px;color:#666;">
				<div class="row">
					<div class="col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right" style="color: #004889">
						<p style="font-size: 50px" class="q-icon-cont"><i class="fas fa-map-marker-alt"></i></p>
						<p style="font-size: 50px" class="q-icon-cont"><i class="far fa-envelope"></i></p>
						<p style="font-size: 50px" class="q-icon-cont"><i class="fas fa-mobile-alt"></i></p>
					</div>
					<div class="col-sm-9 col-md-9 col-lg-9 col-xl-9" style="border:1px solid #D5D5D5">
						<p style="font-size:22px">
							<strong>Cra. 19 N° 35 - 02 Piso 3 Oficina 314 Bucaramanga - Colombia.</strong>
						</p>
						<p style="font-size:22px;margin-top:30px">
							<strong>
								operador.idt@cpcoriente.org
								operador.idt@gmail.com
							</strong>
						</p>
						<p style="font-size:22px;margin-top:30px">
							<strong>
								Tel: 57 + 7 + 6705044 <br>
								Cel: 315 494 0296
							</strong>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- Formulario de  envio de mensaje -->
		<div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 txt-cpc-2">
			<div class="container">
				<form>
					<div class="form-group">
						<label for="">Nombre</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Empresa</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Correo Electrónico</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label for="">Télefono Móvil</label>
						<input type="text" class="form-control">
					</div>
					 <button type="submit" class="btn btn-block" style="background-color:#0099A5;color:#fff">Enviar</button>
				</form>
			</div>
		</div>
	</div>
</div>
<br>
<br>
<br>
<!-- --------------------------------------------------------------------------------------------------------