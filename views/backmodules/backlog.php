<div class="bg-login">
 	<div class="cont-form">
 		<br>
 		<form class="form" id="login" >
      <p align="center" id="response" style="padding: 5px;box-sizing: border-box;background-color: #F48A73;font-size: 12px;display:none;width:100%;border-radius: 5px;margin-top: -25px;">Datos de acceso incorrectos</p>
 			<div class="form-group">
 				<h4 class="text-center txt-cpc-2">Atenea</h4>
 				<input type="email" class="form-control mb-2" name="user" id="user" placeholder="Usuario" onkeypress="EstadoNormal('user')" autocomplete="on" >
 				<input type="password" name="pass" class="form-control mb-4" id="pass" placeholder="Contraseña" onkeypress="EstadoNormal('pass')">
 				<div class="text-center">
 					<button type="submit" class="btn btn-info">Ingresar</button><p  style="margin-top: 10px;font-size: 12px;">
 					<a id="p_olvide" href="index.php?ruta=restaurar_pass" class="text-center txt-cpc-2">Olvide mi contraseña</a></p>
 				</div>
 			</div>

 		</form>
 	</div>
</div>
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Atenea | Inicio de Sesión</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      ..
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>

<script src="views/js/log.js"></script>