<div class="bg-login">
 	<div class="cont-form">
 		<br>
 		<form  class="form" id="restore">
 			 <p align="center" id="response" style="padding: 5px;box-sizing: border-box;background-color: #F48A73;font-size: 12px;display:none;width:100%;border-radius: 5px;margin-top: -25px;">Datos de acceso incorrectos</p>
 			<div class="form-group">
 				<h4 class="text-center txt-cpc-2">Atenea</h4>
 				
 				<input type="email" class="form-control mb-4" id="email" placeholder="Correo electrónico" name="email">
 				<div class="text-center">
 					<button type="submit" class="btn btn-info">Recuperar contraseña</button><br><p  style="margin-top: 10px;font-size: 12px;">
 					<a id="p_olvide" href="index.php?ruta=backlog" class="text-center txt-cpc-2">Volver</a></p>
 					<div id="respuesta"></div>
 				</div>
 			</div>

 		</form>
 	</div>
</div>
<script src="views/js/log.js"></script>