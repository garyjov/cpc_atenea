<?php
/*---------------------------------------------------------------------*/
#Llamado de Archivos 
/*---------------------------------------------------------------------*/
//Limpieza->
require_once "../../security/secur.php";
//Ajax->
require_once "../../ajax/ajax.php";
//DATOS EMPRESA----------------------------------------------------------------------------------------------
//Validar campos enviados Segun peticion Del Java Script---------------------------------------------------->
if(isset($_POST["user"]) && isset($_POST["pass"])){
	//Limpieza de campos con el objeto de seguridad
	$objSeguridad = new Seguridad();
	//Array de datos->				   
	$dtsFiltro = array(	'user'=>$objSeguridad->cleanData($_POST["user"]),
									   	'pass'=>$objSeguridad->cleanData($_POST["pass"])
										);
	
	
	//Instancia del Objeto Ajax
	$objAjax = new Ajax();
	//Paso de datos a la propiedad de datos ajax
	$objAjax->dtsAjax = $dtsFiltro;
	// Ejecucion del metodo de envio
	//Alamacenamiento de la respuesta
	$response = $objAjax->ajaxLogin();
	//Validacion de la respuiesta del controlador pasada a traves del ajax
	echo json_encode($response);
}
//Restaurar Contraseña--------------------------------------------------------------------->
else if(isset($_POST["email"])){

$objSeguridad = new Seguridad();
	//Array de datos->				   
	$dtsFiltro = $objSeguridad->cleanData($_POST["email"]);
	
	//Instancia del Objeto Ajax
	$objAjax = new Ajax();
	//Paso de datos a la propiedad de datos ajax
	$objAjax->dtsAjax = $dtsFiltro;
	// Ejecucion del metodo de envio
	//Alamacenamiento de la respuesta
	$response = $objAjax->ajaxRestore();
	//Validacion de la respuiesta del controlador pasada a traves del ajax
	
	echo $response;

}

