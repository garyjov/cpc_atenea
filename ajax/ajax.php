<?php
#¶►(Llamado): Controlador y Modelo de los datos de registro de empresas◄#
require_once "../../controllers/loginController.php";
require_once "../../models/loginModel.php";

/*---------------------------------------------------------------------------------------------*/
 #Titulo:Ajax de Envio de Datos
/*---------------------------------------------------------------------------------------------*/
 #►Clase: Ajax
 #►Descripcion:Se encarga de enviar los datos al respectivo controlador
 class Ajax{
 	#►Propiedades:
 	public $dtsAjax;
 	//Prpiedad para el id de las empresas
 	public $idEmpresa;
 	#►Metodo: ajax para los datos iniciales de la empresa-------------------------------------->
 	public function ajaxLogin(){
 		//Paso de datos en la propiedad a la variabler de envio 
 		$datosEnvio = $this->dtsAjax;
 		//Envio de datos por medio del controlador
 		//Alamacenamiento de respuesta
 		$response = LoginController::Login($datosEnvio);
 		//Retorno de Respuesta-> 
 		return $response;
 	} 	
 	public function ajaxRestore(){
 		//Paso de datos en la propiedad a la variabler de envio 
 		$email = $this->dtsAjax;
 		//Envio de datos por medio del controlador
 		//Alamacenamiento de respuesta
 		$response = LoginController::Restore($email);
 		//Retorno de Respuesta-> 
 		return $response;
 	} 

 }
 /*Fin------------------------------------------------------------------------------------------*/ 