<?php 
/*---------------------------------------------------------------------------------------------*/
#Titulo: Index del backend
/*---------------------------------------------------------------------------------------------*/
#►Descripcion: Se encarga de mostrar las vistas del backend

#¶►(Controladores): Llamado de Archivos Controller◄#
require_once "controllers/backCotroller.php";
#¶►(Modelos): Llamado de Archivos Model◄#
require_once "models/backModel.php";

#¶►(Comment): Instancia del Objeto controlador del template◄#
$objTemplate = new BackendController();
// Llamado del metodo que traer el template|1
$objTemplate->BackTmpController();
/*Fin-----------------------------------------------------------------------------------------*/