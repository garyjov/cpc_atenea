<?php
/*---------------------------------------------------------------------------------------------*/
#Titulo:CLASE TEMPLATE CONTROLLER
/*---------------------------------------------------------------------------------------------*/
#►Descripcion:Se encarga de enviar las peticiones al modelo para la vista correspondiente
#►Clase: 
class TemplateController{
	#►Variables:

	#►Metodo-1:Trae el template del portal
	public function GetTemplate(){
		//Incluir el modulo del template
		include "views/template.php";
	}
	#►Metodo:Metodo para enviar peticion de modulo para motrar en vista
	public function urlController(){
		// Si esta iniciada la variable de ruta
		if(isset($_GET['ruta'])){
			
			//Alacenar valor de peticion
			$dataSend = $_GET['ruta'];

		}else{//si no

			// Enviar peticion por defecto
			$dataSend = "que";
		}
		// Envio de peticion al modelo 
		// Alamcenamiento Modulo de la respuesta
		$module = TemplateModel::urlModel($dataSend);
		//Incluir el modulo devuelto
		include $module;
	}
	
}
/*Fin------------------------------------------------------------------------------------------*/