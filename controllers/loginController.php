<?php
/*---------------------------------------------------------------------------------------------*/
#Titulo:Clase para las gestiones del usuario
/*---------------------------------------------------------------------------------------------*/
#►Descripcion: Contiene los metos y propiedades para la gestion con el usuario model
class LoginController{

	public function Login($datos){
		//Validacion de Campos iniciados
		if(isset($datos['user']) && isset($datos['pass'])){
			// Restriccion de Caracteres permitidos
			$arrayResp=	array();
		
				//Encriptamiento de Contraseña para verificacion con la de la BBDD
				$datos['pass']=crypt($datos['pass'], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
				//Instacia de la Entidad usuario
				$ObjLogin = new LoginMDL();
				//alamacenar datos de la respuesta - fila buscada
				$response = $ObjLogin->Login($datos, "usuarios");
				//paso de parametros enviados por el modelo para validaciones de acceso
				//Intentos->		

			}//End If Preg match

		return $response;
}
public function Restore($datos){
		//Validacion de Campos iniciados
		
			// Restriccion de Caracteres permitidos
			$arrayResp=	array();
		
				//Instacia de la Entidad usuario
				$ObjRestore = new LoginMDL();
				//alamacenar datos de la respuesta - fila buscada
				$response = $ObjRestore->Restore($datos, "usuarios");
				//paso de parametros enviados por el modelo para validaciones de acceso
				//Intentos->		

			//End If Preg match

		return $response;
	}

		/*Fin------------------------------------------------------------------------------------------*/
}