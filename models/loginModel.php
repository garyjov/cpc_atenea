<?php
//Conexion
require_once "conect.php";
/*---------------------------------------------------------------------------------------------*/
#							Clase Model para el Usuario
/*---------------------------------------------------------------------------------------------*/
#►Descripcion: Se encargara de alamacenar en la bbdd los datos y enviar las respuestas.
#►Clase: UsuarioMDL Herencia 
class LoginMDL extends Conexion{
	/*---------------------------------------------------------------------*/
	//Propiedad para la Conexion interna de la clase
	/*---------------------------------------------------------------------*/
	protected static $cnx_BD;
	/*---------------------------------------------------------------------*/
	#►Metodo para llamar la conexion de forma static
	/*---------------------------------------------------------------------*/
	private static function getConection(){
		self::$cnx_BD = Conexion::ConectarBd();
	}
	/*---------------------------------------------------------------------*/
	#►Metodo para cierre de coexion de forma static
	/*---------------------------------------------------------------------*/
	private static function closeConection(){
		self::$cnx_BD = null;
	}
	
	public function Login($objUsuario, $tabla){
		//Consulta SQL->

		$sqlQuery = "SELECT $tabla.* , roles.nombre as roll FROM $tabla , roles WHERE email = :usu and pass =:pss and $tabla.rol = roles.codigo";

		//Llamado de la conexion
		self::getConection();
		$response=array();
		//Preparacion de la consulta
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//Paso de parametros
		$stmt->bindParam(":usu", $objUsuario['user'], PDO::PARAM_STR);
		$stmt->bindParam(":pss",  $objUsuario['pass'], PDO::PARAM_STR);
		//Ejecucion de la consulta
		
		//Retorno de de la fila encontrada al controller para validaciones
		$stmt->execute();
		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
        	if($fila['bloqueado']==1)
        		{
							$response="usuario bloqueado";
        		}
        	else{
        			$response= array("acceso exitoso",$fila['codigo'],$fila['nombre']);
        			$this->IntentosMDL($objUsuario['user'], "historial_login",1);
                    $this->ResetIntentos($fila['codigo'], "historial_login");
                    setcookie("user_code",$fila['codigo'], time() + (86400 * 30), "/"); 
                    setcookie("user_name",$fila['nombre'], time() + (86400 * 30), "/");
                    setcookie("user_rol",$fila['roll'], time() + (86400 * 30), "/");
                    session_start();
                    $_SESSION['Dash']=true;
        	    }	
        }
        else
        {
					$response= "acceso denegado";
                    $this->IntentosMDL($objUsuario['user'], "historial_login",0);
                    if($this->UsuarioBloqueado($objUsuario['user']))
                    {
                    	$response	="usuario bloqueado";
                    }

        }
	
		//Cierre de conexion
		return $response;
		self::closeConection();
	}
	/*Fin------------------------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Intentos Acceso de Usuario
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion: Se encarga de actualizar los intentos de ingreso al login
	#►Clase: UsuarioMDL
	#►Metodo: IntentosMDL()
	public function IntentosMDL($usu, $tabla,$inte){
		$verificacion=$this->verificarUsuario($usu);	
			if($verificacion[0]){
				if(!$this->RegistroIntentos($verificacion[1]))
					{
						$sqlQuery = "INSERT INTO `$tabla` (`fecha`, `usuario`, `estado_intento`) VALUES (NOW(), :usu, 1)";	
					}
				else{//1-Realizo consulta

						$sqlQuery = "update `$tabla` set  `estado_intento`= estado_intento + 1 where  usuario=:usu and date(fecha) = curdate()";
					}
				//2-Llamo la conexion
				self::getConection();
				//3-Preparo objeto statement
				$stmt = self::$cnx_BD->prepare($sqlQuery);
				//4-Paso de valores
				$stmt->bindParam(":usu", $verificacion[1], PDO::PARAM_STR);
				
			    $stmt->execute();	
		        
				// Cierre de conexion
				self::closeConection();
		        $this->BloqueoMDL($verificacion[1], "usuarios");
		}
	}


	public function verificarUsuario($usu){
		$existe=array();
		$existe[0]=false;
		//1-Realizo consulta
		$sqlQuery = "select * from usuarios where email=:usu";
		//2-Llamo la conexion
		self::getConection();
		//3-Preparo objeto statement
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
	    $stmt->execute();

		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			$existe[0]=true;
			$existe[1]=$fila['codigo'];
		}
		// Cierre de conexion
		return $existe;
		self::closeConection();
	
	}

	public function RegistroIntentos($usu){
		$existe=false;
		//1-Realizo consulta
		$sqlQuery = "select * from historial_login where usuario=:usu and date(fecha) =curdate()";
		//2-Llamo la conexion
		self::getConection();
		//3-Preparo objeto statement
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
	    $stmt->execute();

		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			$existe=true;
		
		}
		// Cierre de conexion
		
		return $existe;
		self::closeConection();
	
	}
	public function UsuarioBloqueado($usu){
		$bloqueado=false;
		//1-Realizo consulta
		$sqlQuery = "select * from usuarios where email=:usu and bloqueado=1";
		//2-Llamo la conexion
		self::getConection();
		//3-Preparo objeto statement
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
	    $stmt->execute();

		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
		  
		  $bloqueado=true;
			
		}
		// Cierre de conexion
		return $bloqueado;
		self::closeConection();
	
	}
	/*Fin------------------------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Bloque de usuarios 
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion: se encarga de bloquear a los usuarios que fallana en el login
	#►Clase:UsuarioMDL
	#►Metodo:BloqueoMDL
	public function BloqueoMDL($usu, $tabla){
		if($this->CantidadIntentos($usu,"historial_login")){
		//1-Generar la consulta
		$sqlQuery = "UPDATE $tabla SET bloqueado = 1 WHERE codigo = :usu";

		//2-Llamado de conexion
		self::getConection();
		//3-Prearar objeto Statemenet
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//4-Paso de parametros
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
		//5-Ejecucion de la consulta, validacion, retorno de respuesta
		$stmt->execute();
		self::closeConection();
		}
	}
	public function ResetIntentos($usu, $tabla){
		
		//1-Generar la consulta
		$sqlQuery = "update $tabla set estado_intento=0 WHERE usuario = :usu and date(fecha) = curdate()";
       // echo "delete from $tabla WHERE usuario = '$usu' and date(fecha) = curdate() and estado_intento=0";
		//2-Llamado de conexion
		self::getConection();
		//3-Prearar objeto Statemenet
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//4-Paso de parametros
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
		//5-Ejecucion de la consulta, validacion, retorno de respuesta
		$stmt->execute();
		self::closeConection();
		
	}
	/*Fin------------------------------------------------------------------------------------------*/
	public function CantidadIntentos($usu, $tabla){	
	     $mayor3=false;
		//1-Generar la consulta
		$sqlQuery = "SELECT estado_intento  FROM $tabla WHERE date(`fecha`)=CURDATE() and usuario=:usu ";
		//echo "SELECT count(*) as aa FROM $tabla WHERE date(`fecha`)=CURDATE() and usuario=$usu and estado_intento=0";
		//2-Llamado de conexion
		self::getConection();
		//3-Prearar objeto Statemenet
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//4-Paso de parametros
		
		$stmt->bindParam(":usu", $usu, PDO::PARAM_STR);
		//5-Ejecucion de la consulta, validacion, retorno de respuesta
		$stmt->execute();
		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
			if($fila['estado_intento']>2)
			$mayor3=true;
		}

		return $mayor3;
		self::closeConection();
		
	}

	public function Restore($objUsuario, $tabla){
		//Consulta SQL->

		$sqlQuery = "SELECT * FROM $tabla WHERE email = :usu ";
		//Llamado de la conexion
		self::getConection();
		$response="";
		//Preparacion de la consulta
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//Paso de parametros
		$stmt->bindParam(":usu", $objUsuario, PDO::PARAM_STR);
		//Ejecucion de la consulta
		
		//Retorno de de la fila encontrada al controller para validaciones
		$stmt->execute();
		if ($fila = $stmt->fetch(PDO::FETCH_ASSOC)) 
		{
    	if($this->ChangePass($objUsuario, "usuarios"))
    		{	
    		 	$response="La nueva contraseña fue enviada a $objUsuario";
    		}
    	else
    		{
    			$response="La contraseña no pudo ser restaurada";	
    		}	
  	}
    else
  	{
   		$response="El correo $objUsuario no existe";
  	}

      return $response;
	}

	public function ChangePass($email, $tabla){
		//Consulta SQL->
		$retorno=false;//Dj98765Go
		$letras=array("Ar","zB","bH","Dj","Ex","uF","Go","pH","wI","mJ","Kq");
        $newPass1=$letras[rand(0,10)].rand(10000,100000).$letras[rand(0,10)];
        $newPass=crypt($newPass1, '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');
		$sqlQuery = "update $tabla  set pass=:new_pass WHERE email = :usu";
		//Llamado de la conexion
		self::getConection();
		$response=array();
		//Preparacion de la consulta
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//Paso de parametros
		$stmt->bindParam(":usu", $email, PDO::PARAM_STR);
		$stmt->bindParam(":new_pass", $newPass, PDO::PARAM_STR);
		//Ejecucion de la consulta
		
		//Retorno de de la fila encontrada al controller para validaciones
		$filas=$stmt->execute();

		if($filas>0)
			{
					$from = "operador.idt@cpcoriente.org";
    				$to = $email;
    				$subject = "Restauración de contraseña";
   					$message = "Hola,\nTu nueva contraseña es : ".$newPass1;
    				$headers = "From:" . $from;
    				if(mail($to,$subject,$message, $headers))
    				{
    					$retorno=true;
    				}
    		
			}
		return $retorno;
	}

}
/*Fin------------------------------------------------------------------------------------------*/