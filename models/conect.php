<?php
//Datos de Conexion
include("config/config.php");
/*---------------------------------------------------------------------------------------------*/
# 									Conexion BBDD
/*---------------------------------------------------------------------------------------------*/
#►Descripcion: Clase para conexiones con las base de datos
#►Metodo: Conection
#►Clase: Conexion
class Conexion{
	//Metodo para Generar y Traer una Conexion
	public static function ConectarBd(){
		//Conexion PDo
		try{
			// Instacio de una nueva pdo
			$ConexionPDO = new PDO("mysql:host=".HOST.";dbname=".DB, USUARIO, PASSWORD);
			//Captura de la Excepcion
			$ConexionPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			//Cambio a caracteres latinos
			$ConexionPDO->exec("SET CHARACTER SET UTF8");
			//Retorno de la Conexion
			return $ConexionPDO;

		}catch(PDOException $e){
			//Captura del mensaje de error
			die("ERROR: ".$e->getMessage());
			//Captura de la linea del error
			echo "Line ERROR: ".$e->getLine();
		}
	}
}
/*Fin------------------------------------------------------------------------------------------*/