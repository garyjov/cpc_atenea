<?php
//Conexion
require_once "conect.php";
/*---------------------------------------------------------------------------------------------*/
#							Clase Model para el Usuario
/*---------------------------------------------------------------------------------------------*/
#►Descripcion: Se encargara de alamacenar en la bbdd los datos y enviar las respuestas.
#►Clase: UsuarioMDL Herencia 
class UsuarioMDL extends Conexion{
	/*---------------------------------------------------------------------*/
	//Propiedad para la Conexion interna de la clase
	/*---------------------------------------------------------------------*/
	protected static $cnx_BD;
	/*---------------------------------------------------------------------*/
	#►Metodo para llamar la conexion de forma static
	/*---------------------------------------------------------------------*/
	private static function getConection(){
		self::$cnx_BD = Conexion::ConectarBd();
	}
	/*---------------------------------------------------------------------*/
	#►Metodo para cierre de coexion de forma static
	/*---------------------------------------------------------------------*/
	private static function closeConection(){
		self::$cnx_BD = null;
	}
	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Registro de Usuarios
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion:Metodo para el registro de un usuario en la base de datos
	public function RegistroMDL($obUsuario, $tabla){
		//Consulta SQL->
		$sqlQuery = "INSERT INTO $tabla (nombres, email, usuario, password, politicas, intentos, bloqueo, id) VALUES (:nm, :em, :us, :ps, :pol, :inten, :blo, 0)";
		//Llamado de la Conexion
		self::getConection();
		//Preparacion de la Consulta
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//Paso de Parametros
		$nm = $obUsuario->getNombre();
		$em = $obUsuario->getEmail();
		$us = $obUsuario->getUsuario();
		$ps = $obUsuario->getPass();
		$pol = $obUsuario->getPoliticas();
		$inten = 0;
		$blo = 0;
		$stmt->bindParam(":nm", $nm, PDO::PARAM_STR);
		$stmt->bindParam(":em", $em, PDO::PARAM_STR);
		$stmt->bindParam(":us", $us, PDO::PARAM_STR);
		$stmt->bindParam(":ps", $ps, PDO::PARAM_STR);
		$stmt->bindParam(":pol", $pol, PDO::PARAM_STR);
		$stmt->bindParam(":inten", $inten, PDO::PARAM_INT);
		$stmt->bindPAram(":blo", $blo, PDO::PARAM_INT);
		//Ejecucion de la Consulta y Validacion de la misma
		if($stmt->execute()){
			//Confirmacion de la ejecucion
			return "success";

		}else{
			//Decline de la ejecucion
			return "error";
		}
		//Cierre de Conexion
		self::closeConection();
	}
	/*Fin------------------------------------------------------------------------------------------*/
	
	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Login de Usuarios
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion: Se encarga de extraer los datos de la BBDD
	#►Clase: UsuarioMDL
	#►Metodo:LoginMDL
	public function LoginMDL($objUsuario, $tabla){
		//Consulta SQL->
		$sqlQuery = "SELECT nombres, usuario, password, intentos, bloqueo FROM $tabla WHERE usuario = :usu";
		//Llamado de la conexion
		self::getConection();
		//Preparacion de la consulta
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//Paso de parametros
		$us = $objUsuario->getUsuario();
		$stmt->bindParam(":usu", $us, PDO::PARAM_STR);
		//Ejecucion de la consulta
		$stmt->execute();
		//Retorno de de la fila encontrada al controller para validaciones
		return $stmt->fetch();
		//Cierre de conexion
		self::closeConection();
	}
	/*Fin------------------------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Intentos Acceso de Usuario
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion: Se encarga de actualizar los intentos de ingreso al login
	#►Clase: UsuarioMDL
	#►Metodo: IntentosMDL()
	public function IntentosMDL($datos, $tabla){
		//1-Realizo consulta
		$sqlQuery = "UPDATE $tabla SET intentos = :inte WHERE usuario = :us";
		//2-Llamo la conexion
		self::getConection();
		//3-Preparo objeto statement
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//4-Paso de parametros
		$us = $datos['usuarioActual'];
		$inte = $datos['nuevosIntentos'];
		$stmt->bindParam(":us", $us, PDO::PARAM_STR);
		$stmt->bindParam(":inte", $inte, PDO::PARAM_INT);
		//5-Ejecucion de la consulta
		//6-Validacion de la Respuesta
		if($stmt->execute()){

			return "success-i";

		}else{

			return "Error-i";

		}
		// Cierre de conexion
		self::closeConection();
	}
	/*Fin------------------------------------------------------------------------------------------*/

	/*---------------------------------------------------------------------------------------------*/
	#Titulo: Bloque de usuarios 
	/*---------------------------------------------------------------------------------------------*/
	#►Descripcion: se encarga de bloquear a los usuarios que fallana en el login
	#►Clase:UsuarioMDL
	#►Metodo:BloqueoMDL
	public function BloqueoMDL($datos, $tabla){
		//1-Generar la consulta
		$sqlQuery = "UPDATE $tabla SET bloqueo = :blo, intentos = :inte WHERE usuario = :us";
		//2-Llamado de conexion
		self::getConection();
		//3-Prearar objeto Statemenet
		$stmt = self::$cnx_BD->prepare($sqlQuery);
		//4-Paso de parametros
		$blo = $datos['blocker'];
		$us = $datos['usuarioActual'];
		$inte = $datos['intentos'];
		$stmt->bindParam(":us", $us, PDO::PARAM_STR);
		$stmt->bindParam(":blo", $blo, PDO::PARAM_INT);
		$stmt->bindParam(":inte", $inte, PDO::PARAM_INT);
		//5-Ejecucion de la consulta, validacion, retorno de respuesta
		if($stmt->execute()){

			return "success-b";
		
		}else{

			return "Error-b";

		}
	}
	/*Fin------------------------------------------------------------------------------------------*/
}
/*Fin------------------------------------------------------------------------------------------*/