<?php 
/*---------------------------------------------------------------------------------------------*/
#Titulo:CLASE PARA EL MODELO TEMPLATE
/*---------------------------------------------------------------------------------------------*/
#►Descripcion:Se encarga de gestionar las peticiones del controlador sobre los links
#►Clase:
class TemplateModel{
	
	#►Variables:
	#►Metodo:Analiza y gestiona las peticiones url del controlador de modulos en la vista
	public function urlModel($data){
		//Lista Blanca de Validación de links permitidos
		if($data == "que" || $data == "como" || $data == "cpc" || $data == "cuando" || $data == "contacto" ){
		
			//Alacenar el modulo pedido
			$moduleSend = "views/modules/".$data.".php";
		
		}else if($data == "backlog" || $data == "restaurar_pass"){
			
			//Alamacenar el modulo del backend
			$moduleSend = "views/backmodules/".$data.".php";

		}

		else{

			//Retorno del modulo por defecto
			$moduleSend = "views/modules/que.php";

		}

		//Retorno del modulo
		return $moduleSend;
	
	}
	
}
/*Fin------------------------------------------------------------------------------------------*/